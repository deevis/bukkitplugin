package org.javatech.minecraft.hellobukkitplugin;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class HelloBukkitPlugin extends JavaPlugin {

	public void onEnable(){
		getLogger().info("onEnable has been invoked!");
		getServer().getPluginManager().registerEvents(new HelloBukkitListener(this), this);
		getCommand("moira").setExecutor(new MoiraCommandExecutor(this));
		getCommand("natalie").setExecutor(new NatalieCommandExecutor(this));
		getCommand("nathan").setExecutor(new NathanCommandExecutor(this));

    getCommand("hut").setExecutor(new HutCommandExecutor(this));

	}
 
	public void onDisable(){
		getLogger().info("onDisable has been invoked!");
	}

	
	
}
