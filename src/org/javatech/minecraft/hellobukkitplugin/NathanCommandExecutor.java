package org.javatech.minecraft.hellobukkitplugin;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class NathanCommandExecutor implements CommandExecutor {

	private HelloBukkitPlugin plugin; // pointer to your main class, unrequired if you don't need methods from the main class
	 
	public NathanCommandExecutor(HelloBukkitPlugin plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if ( sender instanceof Player) {
			Player p = ((Player)sender);
			p.sendMessage("You will turn into a monster on Halloween!!!");
			return true;
		}
		return false;
	}
	
	

}
