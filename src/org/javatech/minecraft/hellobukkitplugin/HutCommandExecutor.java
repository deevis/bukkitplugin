package org.javatech.minecraft.hellobukkitplugin;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class HutCommandExecutor implements CommandExecutor {

	private HelloBukkitPlugin plugin; // pointer to your main class, unrequired if you don't need methods from the main class
	 
	public HutCommandExecutor(HelloBukkitPlugin plugin) {
		this.plugin = plugin;
	}

	private static Map<String,Integer> materialNames = new HashMap<String,Integer>();
	static {
	  Map<String,Integer> m = materialNames;   // alias for brevity
	  m.put("stone", 1);
	  m.put("cobblestone", 4);
	  m.put("wood", 17);
	  m.put("glass", 20);
	  m.put("brick", 45);
    m.put("gold", 41);
    m.put("iron", 42);
	  m.put("lapis", 22);
	  m.put("moss", 48);
	  m.put("obsidian",49);
	}
	
	//usage: /hut [stone|cobblestone|wood|glass|brick|gold|iron|lapis|moss|obsidian :wood] [height :4] [length :5] [width :5] [stories :1]
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
	  StringBuilder sb = new StringBuilder();
	  for ( String s : args ) {
	    sb.append(s).append(" ");
	  }
	  this.plugin.getLogger().info("Running: /hut " + sb.toString());
	  int[] materials = new int[]{17};
	  int storyHeight = 4;
		int length = 4;
		int width = 4;
		int stories = 1;
	  
		if ( sender instanceof Player) {
		  Player p = ((Player)sender);
			try {
  		  if (args.length > 0) {
  			  String[] parts = args[0].split("\\|");
  			  materials = new int[parts.length];
  			  for ( int i = 0; i < parts.length; i++ ) {
  			    Integer lookup = materialNames.get(parts[i]);
  			    if ( lookup == null ) {
  			      throw new RuntimeException("Unknown material : " + parts[i]);
  			    }
  			    materials[i] = lookup;
  			  }
  			}
        if (args.length > 1) {
          storyHeight = Integer.parseInt(args[1]);
        }
        if (args.length > 2) {
          length = Integer.parseInt(args[2]);
        }
        if (args.length > 3) {
          width = Integer.parseInt(args[3]);
        }
        if (args.length > 4) {
          stories = Integer.parseInt(args[4]);
        }
			} catch ( Exception e ) {
			  e.printStackTrace();
			  p.sendMessage(e.getLocalizedMessage());
			  p.sendMessage("usage: /hut [stone|cobblestone|wood|glass|brick|gold|iron|lapis|moss|obsidian :wood] [height :4] [length :4] [width :4] [stories :1]");
			  return true;
			}
			Location l = p.getLocation();
      this.plugin.getLogger().info(String.format("getXYZ(%s,%s,%s)",l.getX(), l.getY(), l.getZ()));
			this.plugin.getLogger().info(String.format("getBlock(%s,%s,%s)",l.getBlockX(), l.getBlockY(), l.getBlockZ()));
	    int startX = l.getBlockX() - length/2;
      int startY = l.getBlockY();
      int startZ = l.getBlockZ() - width/2;
	    World world = p.getWorld();
			for (int story = 0; story < stories; story++ ) {
			  for (int y = 0; y < storyHeight-1; y++) {
			    buildRectangle(world, materials, startX, startY + (story * storyHeight) + y, startZ, length, width);
			  }
        filledRectangle(world, materials, startX, startY + ((story + 1) * storyHeight -1), startZ, length, width);
			}
			// TODO: Put windows on each level
			
			// TODO: Put door on bottom level
			
			// TODO: Put torches on each level
			
			return true;
		}
		return false;
	}

  private void buildRectangle(World world, int[] materials, int x, int y, int z, int xLength, int zWidth) {
    buildLine(world, materials, y, x, z, x + xLength, z );
    buildLine(world, materials, y, x + xLength, z, x + xLength, z + zWidth );
    buildLine(world, materials, y, x + xLength, z + zWidth, x, z + zWidth );
    buildLine(world, materials, y, x, z + zWidth, x, z );
  }

  private void filledRectangle(World world, int[] materials, int x, int y, int z, int xLength, int zWidth) {
    for ( int i = 0; i <= xLength; i++ ) {
      buildLine(world, materials, y, x+i, z, x+i, z + zWidth);
    }
  }

  private void buildLine(World world, int[] materials, int y, int startX, int startZ, int endX, int endZ) {
    this.plugin.getLogger().info(String.format("buildLine(%s,%s,%s,%s,%s)", y, startX, startZ, endX, endZ));
    // make sure startX is smaller than endX, and that startZ is smaller than endZ - or else the loops won't be entered...
    if ( startX > endX ) {
      int temp = endX;
      endX = startX;
      startX = temp;
    }
    if ( startZ > endZ ) {
      int temp = endZ;
      endZ = startZ;
      startZ = temp;
    }
    for (int x = startX; x <= endX; x++) {
      for (int z = startZ; z<= endZ; z++ ) {
        int materialIndex=(Math.abs(x+y+z))%materials.length;
        world.getBlockAt(x, y, z).setTypeId(materials[materialIndex]);
        //this.plugin.getLogger().info(String.format("getBlockAt(%s,%s,%s).setTypeId(%s)", x, y, z, 17));
      }
    }
  }

}


