package org.javatech.minecraft.hellobukkitplugin;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MoiraCommandExecutor implements CommandExecutor {

	private HelloBukkitPlugin plugin; // pointer to your main class, unrequired if you don't need methods from the main class
	 
	public MoiraCommandExecutor(HelloBukkitPlugin plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
			if ( sender instanceof Player) {
				Player p = ((Player)sender);
				sender.sendMessage("Where have you been, I've been waiting and waiting and waiting for hours?!?");
				p.getWorld().strikeLightning(p.getLocation());
			}
			return true;
	}

}



