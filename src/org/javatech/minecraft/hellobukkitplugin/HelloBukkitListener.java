package org.javatech.minecraft.hellobukkitplugin;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class HelloBukkitListener implements Listener {

	private HelloBukkitPlugin plugin; // pointer to your main class, unrequired if you don't need methods from the main class
	 
	public HelloBukkitListener(HelloBukkitPlugin plugin) {
		this.plugin = plugin;
	}


	@EventHandler
	public void playerJoined(PlayerJoinEvent event) {
		event.getPlayer().sendMessage("Welcome " + event.getPlayer().getName() + "!  We have WorldEdit, Hyperconomy and Essentials mods running...have fun...grief at your own risk...");
	}
}
